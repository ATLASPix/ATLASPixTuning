from Tune import *
from TOT import *


#VNDACs to process
VNDAC = 0
TDAC = 0
vmax=0.5
npoints=21
target=0.16
device = "ap1b02w23s21"
vnpix=45
vnfb = 20

PlotTOTs(device,"/home/peary/peary/PEARYDATA/%s/TOT_VNFBPix%i_VNPix%i.txt"%(device,vnfb,vnpix),1.2,26,vnfb,vnpix)
PlotTDACs(device, VNDAC, TDAC, "/home/peary/peary/PEARYDATA/%s/SCURVE_VNDAC%i_TDAC%i.txt" % (device, VNDAC, TDAC), vmax,npoints)
