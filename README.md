Tune.py : Contains function to plot results from DoSCurves or TDACScan, extract the thresholds for each pixels and calculate the tuning of multiple TDAC have been scanned
   * PlotTDACs : Generate the SCurves plots for a given TDAC and VNDAC
   * tune : Fits the threshold vs TDAC and select the best TDAC for each pixel given a target value

TOT.py : Contains the function to plot the results from MeasureTOT command 
   * PlotTOT : Plot TOT vs pulse height for each pixels plus some associated plots
   
FindNoisyPixels.py : Utility to generate hitmap from a raw data file, identify hot pixels and generate the command to mask them 


