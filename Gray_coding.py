def gray_encode(n):

    return n ^ (n >> 1)


def gray_decode(n) :
    n2 = n
    n= n >> 1
    while (n):
        n2 ^= n
        n= n >> 1
    return n2

def ivan_gray_decode(n,l):
    normal = n & (1 << (l-1))
    for i in range(l-2,-1,-1):
        #print i
        normal |= (n ^ (normal >> 1)) & (1 << i)
    return normal


def decode_tot(ts1,ts2,ckdivend2):
        totpix = ts2 & 0x3F
        tss = (((ts1)*2/ckdivend2) & 0x3F)
        if totpix<=tss:
            totpix = 64 + totpix -tss
            #print "TOT: %i TSS: %i TOTPIX: %i "%(totpix,tss,ts2 & 0x3F)
        else:
            totpix =totpix- tss
            #print "TOT: %s rolled"%bin(totpix)

        return totpix


if __name__ == '__main__':
    for i in range(128):
        #print "for %i"%i
        gray=gray_encode(i)
        print bin(gray)

        #print "binary : ", bin(i), " gray: " ,bin(gray)
        decoded=gray_decode(gray&0x3FF)
        decoded_ivan = ivan_gray_decode(gray&0x3FF,11)
        print decoded,decoded_ivan
        # print "binary : ", bin(i), " gray: " ,bin(decoded)
        # if(i==decoded):
        #     print "match"



