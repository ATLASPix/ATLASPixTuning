from ROOT import *
import math
from array import array
import numpy
import os

gROOT.SetBatch(True)


def PlotTOTs(device,filename,vmax=0.8,npoints=20,vnfb=20,vnpix=20):

    f = open(filename)
    lines = f.readlines()
    grs = []
    dv = (vmax - 0) / (npoints - 1)

    schisto = TH2D("TOTPersistentHisto", "", npoints-1, dv, vmax, 400, 1, 64)

    tothistos={}
    for j in range(npoints):
            tothistos["%f"%(j*dv)]= TH2D("TOT_vp=%f"%(j*dv),"",25,0,25,400,0,400)

    hista = TH1D("hist_a","hist_a",1000,-500,500)
    histb = TH1D("hist_b","hist_b",1000,-500,500)
    histc = TH1D("hist_c","hist_c",1000,-500,500)
    histd = TH1D("hist_d","hist_d",1000,-500,500)

    for line in lines[0:]:
        words = line.split()

        v = []
        cnt = []
        #print line
        col = int(words[0])
        row = int(words[1])
        Vact = 0


        for j in range(npoints):
            v.append(Vact)
            cnt.append(float(words[2 + j]))
            tothistos["%f"%Vact].Fill(col,row,float(words[2 + j]))
            Vact+=dv

        gr = TGraph(npoints)
        gr.SetName("TOT_%i_%i"%(col,row))

        for i, iv in enumerate(v):
            gr.SetPoint(i, v[i], cnt[i])
            schisto.Fill(v[i], cnt[i])

        func = TF1("TOTFIT_%i_%i" % (col, row), "pol3", 0, vmax)
        #func.SetParLimits(0, 0, 64)
        #func.SetParLimits(1, 0, 1000)
        #func.SetParLimits(1, 0, 50)
        #func.SetParLimits(2, 0, vmax)

        gr.Fit(func, "RMQ")

        hista.Fill(func.GetParameter(0))
        histb.Fill(func.GetParameter(1))
        histc.Fill(func.GetParameter(2))
        histd.Fill(func.GetParameter(3))

        grs.append(gr)


    canschisto = TCanvas()
    schisto.Draw("colz")

    outfile = TFile("/home/peary/peary/SamplePlots/TOT_%s_VNFB%i_VNPix%i.root"%(device,vnfb,vnpix),"update")
    schisto.Write()

    for j in range(npoints):
            tothistos["%f"%(j*dv)].Write()

    hista.Write()
    histb.Write()
    histc.Write()
    histd.Write()

    outfile.mkdir("single pixel tot")
    outfile.cd("single pixel tot")
    [gr.Write() for gr in grs]
    outfile.Close()




if __name__ == '__main__':

    device = "ap1b02w06s11"
    vnpix=40
    vnfb = 20
    PlotTOTs(device,"/home/peary/peary/PEARYDATA/%s/TOT_VNFBPix%i_VNPix%i.txt"%(device,vnfb,vnpix),1.2,26,vnfb,vnpix)