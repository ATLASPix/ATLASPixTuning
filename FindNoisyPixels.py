from ROOT import *
import math
from array import array
from Gray_coding import *

basefolder = "../PEARYDATA/test"


def HitMap(datafile,HP):

    histo = TH2I("noisehits","noisehits",25,0,25,400,0,400)
    tot = TH1D("","",128,0,128)
    timeline = TH1D("","",1024,0,1024)
    dataf=open(datafile)

    datal=dataf.readlines()
    datal=[line for line in datal if "X:" not in line]
    datal=[line for line in datal if "WEIRD" not in line]
    datal=[line for line in datal if "SERDES" not in line]
    datal=[line for line in datal if "BUSY" not in line]

    for line in datal:
        words=line.split()
        #print words
        col=int(words[1])
        row=int(words[2])
        #ts1=gray_decode(int(words[2])& 0x3FF)#+(int(words[2])&0x300)
        #ts2=gray_decode(int(words[3])& 0x3F)
        #totpix=decode_tot(ts1,ts2,16)

        if([col,row] not in HP) :
            #tot.Fill(totpix)
            #timeline.Fill(ts1)
            histo.Fill(col,row)

    return histo,timeline,tot

def getHPList(histo,thres):

    HP = []
    for i in range(histo.GetNbinsX()+1) :
        for j in range(histo.GetNbinsY()+1):
            if(histo.GetBinContent(i,j)>=thres):
                #print "%i,%i %i"%(i-1,j-1,histo.GetBinContent(i,j))
                HP.append([i-1,j-1])
    print "# of hot pixels : %i"%(len(HP))
    return HP

def writeTDACFile(HP,filename):

    f=open(filename,"w")
    for i in range(25) :
        for j in range(400):
            if([i,j] in HP):
                f.write("%i %i %i %i\n"%(i,j,4,1))
            else:
                f.write("%i %i %i %i\n"%(i,j,4,0))

def genGUICmd(HP):
    print HP
    for i in range(25) :
        for j in range(400):
            if[i,j] in HP:
                print "MaskPixel %i %i 0"%(i,j)

if __name__ == '__main__':
    histo,timeline,tot=HitMap("%s/data.txt"%basefolder,[])
    HP=getHPList(histo,100)
    histo,timeline,tot=HitMap("%s/data.txt"%basefolder,HP)

    can = TCanvas()
    can.Draw()
    histo.Draw("colz")
    can2 = TCanvas()
    can2.Draw()
    timeline.Draw()
    #
    can3 = TCanvas()
    can3.Draw()
    tot.Draw()

    writeTDACFile(HP,"GradA03_TDAC.cfg")
    genGUICmd(HP)

    a=raw_input()
