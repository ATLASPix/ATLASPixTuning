import ROOT
import math
from array import array
import numpy
import os
import TOTAna
#gROOT.SetBatch(True)




def PlotTDACs(device,VNDAC, TDAC, filename,vmax=0.8,npoints=20,scale=1,n_pulses=100,rootfile=0):

    f = open(filename)
    lines = f.readlines()
    grs = []


    schisto = ROOT.TH2D("SCurveHisto_TDAC%i"%TDAC, "", 40, 0, vmax, 400, 0, 400)

    tree = ROOT.TTree("tunetree_TDAC%i"%TDAC,"tunetree")


    col=0
    row=0
    colt = array( 'i', [ 0 ] )
    rowt = array( 'i', [ 0 ] )
    th= array( 'f', [ 0 ] )
    noise = array( 'f', [ 0 ] )
    tdac = array( 'i', [ TDAC ] )
    Chi2= array( 'f', [ 0 ] )


    tree.Branch('col', colt, 'col/I')
    tree.Branch('row', rowt, 'row/I')
    tree.Branch('TDAC', tdac, 'row/I')
    tree.Branch('th', th, 'row/F')
    tree.Branch('noise', noise, 'row/F')
    tree.Branch('Chi2', Chi2, 'Chi2/F')

    dv = (vmax - 0) / (npoints - 1)

    for line in lines[0:]:
        words = line.split()

        v = []
        cnt = []
        #print line
        col = int(words[0])
        row = int(words[1])

        Vact = 0
        for j in range(npoints):
            v.append(Vact)
            cnt.append(int(words[2 + j]))
            Vact+=dv

        gr = ROOT.TGraph(npoints)
        gr.SetName("SCurve_%i_%i"%(col,row))

        for i, iv in enumerate(v):
            gr.SetPoint(i, v[i], cnt[i])
            schisto.Fill(v[i], cnt[i])

        grs.append(gr)

    canschisto = ROOT.TCanvas()
    schisto.Draw("colz")

    can = ROOT.TCanvas()

    ths = []

    npulses = n_pulses

    ThMap = ROOT.TH2D("THMAP_TDAC%i"%TDAC,"",25,0,25,400,0,400)
    noiseMap = ROOT.TH2D("NoiseMAP_TDAC%i"%TDAC,"",25,0,25,400,0,400)
    Chi2Map = ROOT.TH2D("Chi2MAP_TDAC%i"%TDAC,"",25,0,25,400,0,400)

    col=0
    row=0
    for i, gr in enumerate(grs):
        if i == 0:
            gr.Draw("LA PLC")
        else:

            gr.Draw("LC PLC")

        name=gr.GetName()
        w=name.split("_")
        col=int(w[1])
        row=int(w[2])

        func = ROOT.TF1("Erf", "[2]*TMath::Erf([0]*(x-[1]))+[3]", 0, vmax)
        func.SetParameter(0, 0.05)
        func.SetParameter(1, 0.25)
        func.SetParameter(2, npulses / 2)
        func.SetParameter(3, npulses / 2)
        func.SetParLimits(0, 0, 50)
        func.SetParLimits(1, 0, vmax)
        func.SetParLimits(2, npulses / 2 - 100, npulses / 2 + 100)
        #func.SetParLimits(3, npulses / 2 - 100, npulses / 2 + 100)
        gr.Fit(func, "RMQ")
        ths.append(func.GetParameter(1))

        ThMap.Fill(col,row,func.GetParameter(1))
        noiseMap.Fill(col,row,func.GetParameter(0))
        Chi2Map.Fill(col,row,func.GetChisquare())

        colt[0]=col
        rowt[0]=row
        th[0]=func.GetParameter(1)
        noise[0]=func.GetParameter(0)
        Chi2[0]=func.GetChisquare()

        tree.Fill()


    can2 = ROOT.TCanvas()

    h = ROOT.TH1D("ThHisto_VNDAC%i_TDAC%i"%(VNDAC,TDAC), "", 100, 0, vmax*scale)

    for th in ths:
        h.Fill(th*scale)

    h.Draw("")

    if rootfile==0:
        outfile = ROOT.TFile("/home/peary/peary/SamplePlots/Tuning_%s_VNDAC%i.root"%(device,VNDAC),"update")
    else :
        outfile=rootfile

    schisto.Write()
    h.Write()
    noiseMap.Write()
    ThMap.Write()
    Chi2Map.Write()
    tree.Write()
    #outfile.mkdir("Fit_TDAC%i"%TDAC)
    #outfile.cd("Fit_TDAC%i"%TDAC)
    #[gr.Write() for gr in grs]
    outfile.Close()



def tune(rootfile, vndac, tdacs, target=0.45,vmax=0.8):
    f = rootfile

    his = []
    for tdac in tdacs:
        hist = f.Get("THMAP_TDAC%i" % (tdac))
        his.append(hist)
        # hist.Draw()
        # a=raw_input()

    grs = []
    lins = []

    f.mkdir("fits")
    f.cd("fits")

    hist_best = ROOT.TH1D("best tdacs", "", 8, 0, 8)
    ideal_th = ROOT.TH1D("best th", "", 400, 0.1, vmax)
    ThMap = ROOT.TH2D("THMAP_tunedExp", "", 25, 0, 25, 400, 0, 400)
    TdacMap = ROOT.TH2D("TdacMAP_tunedExp", "", 25, 0, 25, 400, 0, 400)
    Chi2Map = ROOT.TH2D("Chi2MAP_linfit", "", 25, 0, 25, 400, 0, 400)

    tunefile = open("tune_vndac%i_target%f.txt" % (vndac, target), "w")

    can = ROOT.TCanvas()
    print "Target %f V"%target

    print his

    for col in range(25):
        grs.append([])
        lins.append([])
        print "Processing column %i"%col
        for row in range(400):
            ths = [h.GetBinContent(h.GetBin(col + 1, row + 1)) for h in his if
                   h.GetBinContent(h.GetBin(col + 1, row + 1)) != vmax]
            gr = ROOT.TGraph(len(tdacs))
            gr.SetName("pix%i_%i" % (col, row))
            # print ths
            [gr.SetPoint(i, tdacs[i], th) for i, th in enumerate(ths) if th<vmax]

            func = ROOT.TF1("lin%i_%i" % (col, row), "pol3", 0, 7)
            gr.Fit(func, "RMQ")

            best_tdac = func.GetX(target)

            diff = ([abs(th-target) for th in ths])
            #print diff
            idth = diff.index(min(diff))

            best = tdacs[idth]#int(math.floor(best_tdac))

            #print best
            hist_best.Fill(best)
            tunefile.write("%i %i %i %i\n" % (col, row, best, 0))

            best_th = func.Eval(best_tdac)
            #print best, best_th
            ThMap.Fill(col, row, best_th)
            TdacMap.Fill(col, row, best_tdac)
            Chi2Map.Fill(col, row, func.GetChisquare())
            ideal_th.Fill(best_th)
            grs[col].append(gr)
            lins[col].append(func)

            can.cd()
            if (col == 0 and row == 0):
                gr.Draw()
            else:
                gr.Draw("same")

            gr.Write()
            func.Write()
    f.cd()
    hist_best.Write()
    ideal_th.Write()
    can.Write()
    ThMap.Write()
    TdacMap.Write()
    Chi2Map.Write()
    f.Close()
    tunefile.close()

if __name__ == '__main__':

    #VNDACs to process
    vndacs = [32]
    tdacs = [0,1,2]
    vmax=0.4
    npoints=41
    target=0.2
    device = "ap1b02w23s13"

    calibfactor = 1600/0.3

    #Process TDAC S-Curve files for tuning
    for VNDAC in vndacs:
       print "processing VNDAC %i"%VNDAC
       #os.system("rm Tuning_VNDAC%i.root" % VNDAC)
       for TDAC in tdacs:
           print "processing TDAC %i"%TDAC
           PlotTDACs(device,VNDAC, TDAC, "/home/peary/peary/PEARYDATA/%s/SCURVE_VNDAC%i_TDAC%i.txt"%(device,VNDAC,TDAC),vmax,npoints)
       #tune("/home/peary/peary/SamplePlots/Tuning_%s_VNDAC%i.root"%(device,VNDAC),VNDAC,tdacs,target,vmax)
    #
    # #Compute Tuned TDAC`
    # for VNDAC in vndacs:
    #     print "tuning VNDAC %i"%VNDAC
    #     #for target in numpy.arange(0.28,0.32,0.01):
    #     tune("Tuning_VNDAC%i.root"%VNDAC,VNDAC,tdacs,target)

    # Process SCurve file from tuning verification
    # for VNDAC in vndacs:
    #   print "verifying VNDAC %i"%VNDAC
    #PlotTDACs(device,45, 10, "/home/peary/peary/PEARYDATA/%s/SCURVE_TDAC_verification.txt"%device,vmax,npoints)
